/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

/**
 *
 * @author Lenovo
 */
public class SzyfrCezara {

    public SzyfrCezara() {
    }

    public static String Szyfr(String cText, int key) {
        char[] text = cText.toLowerCase().toCharArray();
        StringBuilder cipher = new StringBuilder();
        for (int i = 0; i < text.length; i++) {
            char znak = (char) (((int)text[i] + key - 65) % 26 + 65);
            cipher.append(znak);
        }
        return cipher.toString();
    }

    public static String unSzyfr(String dText, int key) {
        char[] text = dText.toLowerCase().toCharArray();
        StringBuilder decipher = new StringBuilder();
        key = 26 - key;
        for (int i = 0; i < text.length; i++) {
                char znak = (char) (((int) text[i] + key - 65) % 26 + 65);
                decipher.append(znak);            
        }

        return decipher.toString();
    }
}
