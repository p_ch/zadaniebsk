package app;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.BitSet;
import java.util.Set;
import java.util.TreeSet;
import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class krypto1 extends Application {

    private String mainTitle = "Bezpieczeństwo sieci komputerowych - zadanie 1 i 2";
    // main - główne okno do wyświetlania zawartości
    FlowPane main;

    @Override
    public void start(Stage primaryStage) {

        // inicjalizacja menu aplikacji
        MenuBar menubar = new MenuBar();
        Menu menu1 = new Menu("Menu");
        MenuItem menuitem1 = new MenuItem("Szyfrowanie Rail fence");
        // akcja po wybraniu elementu 1 menu
        menuitem1.setOnAction(e -> {
            primaryStage.setTitle(mainTitle + " - Rail fence");
            if (!main.getChildren().isEmpty()) {
                main.getChildren().clear();
            }
            // tworzenie layoutu do zadania
            LayoutRailFence();
        });
        MenuItem menuitem2 = new MenuItem("Przestawianie macierzowe sposób 1");
        // akcja po wybraniu elementu 2 menu
        menuitem2.setOnAction(e -> {
            primaryStage.setTitle(mainTitle + " - Przestawianie macierzowe sposób 1");
            // czyszczenie okna
            if (!main.getChildren().isEmpty()) {
                main.getChildren().clear();
            }
            // tworzenie layoutu do zadania
            LayoutPrzestawianieMacierzowe();
        });
        
        MenuItem menuitem3 = new MenuItem("Przestawianie macierzowe sposób 2");
        // akcja po wybraniu elementu 2 menu
        menuitem3.setOnAction(e -> {
            primaryStage.setTitle(mainTitle + " - Przestawianie macierzowe sposób 2");
            // czyszczenie okna
            if (!main.getChildren().isEmpty()) {
                main.getChildren().clear();
            }
            // tworzenie layoutu do zadania
            LayoutPrzestawianieMacierzowe2();
        });
        
        MenuItem menuitem4 = new MenuItem("Przestawianie macierzowe sposób 3");
        // akcja po wybraniu elementu 2 menu
        menuitem4.setOnAction(e -> {
            primaryStage.setTitle(mainTitle + " - Przestawianie macierzowe sposób 3");
            // czyszczenie okna
            if (!main.getChildren().isEmpty()) {
                main.getChildren().clear();
            }
            // tworzenie layoutu do zadania
            LayoutPrzestawianieMacierzowe3();
        });
        
        MenuItem menuitem5 = new MenuItem("Szyfrowanie Cezara");
        
        menuitem5.setOnAction(e -> {
            primaryStage.setTitle(mainTitle + " - Szyfrowanie Cezara");
            if (!main.getChildren().isEmpty()) {
                main.getChildren().clear();
            }
            // tworzenie layoutu do zadania
            LayoutSzyfrCezara();
        });
        MenuItem menuitem6 = new MenuItem("Szyfrowanie Vigenere'a");
       
        menuitem6.setOnAction(e -> {
            primaryStage.setTitle(mainTitle + " - Szyfrowanie Vigenere'a");
            if (!main.getChildren().isEmpty()) {
                main.getChildren().clear();
            }
            // tworzenie layoutu do zadania
            LayoutSzyfrowanieVigenere();
        });
        
               
        
        menu1.getItems().add(menuitem1);
        menu1.getItems().add(menuitem2);
        menu1.getItems().add(menuitem3);
        menu1.getItems().add(menuitem4);
        menu1.getItems().add(menuitem5);
        menu1.getItems().add(menuitem6);
        menubar.getMenus().add(menu1);
                // główny layout aplikacji z implementacją menu
        VBox root = new VBox(menubar);

        // main - główne okno do wyświetlania zawartości
        main = new FlowPane();
        main.setPadding(new Insets(30, 30, 30, 30));
        root.getChildren().add(main);

        // tworzenie okna aplikacji
        Scene scene = new Scene(root, 600, 400);
        primaryStage.setTitle(mainTitle);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    public void LayoutRailFence() {
        Label plainTextLabel = new Label("Tekst do zaszyfrowania/odszyfrowania:");
        Label keyLabel = new Label("Podaj liczbę n:");
        Label cipherTextLabel = new Label("Postać zaszyfrowana/odszyfrowana:");
        Label cipherText = new Label(" - pusty - ");
        TextField plainTextField = new TextField();
        plainTextField.setPadding(new Insets(15, 15, 15, 15));
        TextField keyTextField = new TextField("3");
        keyTextField.setPadding(new Insets(15, 15, 15, 15));
        keyTextField.prefWidth(60);
        HBox keyBox = new HBox();
        keyBox.setSpacing(20);
        keyBox.setAlignment(Pos.CENTER_LEFT);
        keyBox.getChildren().add(keyLabel);
        keyBox.getChildren().add(keyTextField);
        Button doCipher = new Button("Szyfrowanie");
        doCipher.setOnAction(c -> {
            String plainText = plainTextField.getText();
            plainText = plainText.replaceAll("\\s", "");
            int key = Integer.parseInt(keyTextField.getText());
            String cipher = RailFence.szyfrowanie(plainText, key);
            cipherText.setText(cipher);
        });
        Button deCipher = new Button("Odszyfrowywanie");
        deCipher.setOnAction(c -> {
            String plainText = plainTextField.getText();
            plainText = plainText.replaceAll("\\s", "");
            int key = Integer.parseInt(keyTextField.getText());
            String cipher = RailFence.deszyfrowanie(plainText, key);
            cipherText.setText(cipher);
        });
        
        keyBox.setSpacing(20);
        keyBox.setAlignment(Pos.CENTER_LEFT);
        keyBox.getChildren().add(doCipher);
        keyBox.getChildren().add(deCipher);
        
        main.setOrientation(Orientation.VERTICAL);
        main.setVgap(15);
        main.getChildren().add(plainTextLabel);
        main.getChildren().add(plainTextField);
        main.getChildren().add(keyBox);
        main.getChildren().add(cipherTextLabel);
        main.getChildren().add(cipherText);
    
    }

        public void LayoutPrzestawianieMacierzowe() {
        Label plainTextLabel = new Label("Tekst do zaszyfrowania / rozszyfrowania:");
        Label keyLabel = new Label("Klucz w postacji x-x-...");
        Label cipherLabel = new Label("Postać zaszyfrowana / odszyfrowana:");
        Label cipherText = new Label(" - pusty -");
        TextField plainTextField = new TextField();
        plainTextField.setPadding(new Insets(15, 15, 15, 15));
        TextField keyField = new TextField("2-3-1");
        keyField.setPadding(new Insets(15, 15, 15, 15));
        Button doCipher = new Button("Szyfrowanie");
        Button deCipher = new Button("Odszyfrowywanie");
        doCipher.setOnAction(c -> {
            String key = keyField.getText();
            String plainText = plainTextField.getText();
            plainText = plainText.replaceAll("\\s", "");
            String cipher = Matrix.szyfr(plainText, key);
            cipherText.setText(cipher);
        });
        deCipher.setOnAction(c -> {
            String key = keyField.getText();
            String deCipherText = plainTextField.getText();
            deCipherText = deCipherText.replaceAll("\\s", "");
            String plain = Matrix.deszyfr(deCipherText, key);
            cipherText.setText(plain);
        });
                     
        HBox keyBox = new HBox();
        keyBox.setSpacing(20);
        keyBox.setAlignment(Pos.CENTER_LEFT);
        keyBox.getChildren().add(doCipher);
        keyBox.getChildren().add(deCipher);
        
        main.setOrientation(Orientation.VERTICAL);
        main.setVgap(20);
        main.getChildren().add(plainTextLabel);
        main.getChildren().add(plainTextField);
        main.getChildren().add(keyLabel);
        main.getChildren().add(keyField);
        main.getChildren().add(cipherLabel);
        main.getChildren().add(cipherText);
        main.getChildren().add(keyBox); 
        
    }
    
    public void LayoutPrzestawianieMacierzowe2() {
        Label plainTextLabel = new Label("Tekst do zaszyfrowania / rozszyfrowania:");
        Label keyLabel = new Label("Klucz (słowo): ");
        Label cipherLabel = new Label("Postać zaszyfrowana / odszyfrowana:");
        Label cipherText = new Label(" - pusty -");
        TextField plainTextField = new TextField();
        plainTextField.setPadding(new Insets(15, 15, 15, 15));
        TextField keyField = new TextField();
        keyField.setPadding(new Insets(15, 15, 15, 15));
        Button doCipher = new Button("Szyfrowanie");
        Button deCipher = new Button("Odszyfrowywanie");
        doCipher.setOnAction(c -> {
            String key = keyField.getText();
            String plainText = plainTextField.getText();
            plainText = plainText.replaceAll("\\s", "");
            String cipher = Matrix.szyfruj(plainText, key);
            cipherText.setText(cipher);
        });
        deCipher.setOnAction(c -> {
            String key = keyField.getText();
            String deCipherText = plainTextField.getText();
            deCipherText = deCipherText.replaceAll("\\s", "");
            String plain = Matrix.deszyfruj(deCipherText, key);
            cipherText.setText(plain);
        });
        
                
        HBox keyBox = new HBox();
        keyBox.setSpacing(20);
        keyBox.setAlignment(Pos.CENTER_LEFT);
        keyBox.getChildren().add(doCipher);
        keyBox.getChildren().add(deCipher);
        
        main.setOrientation(Orientation.VERTICAL);
        main.setVgap(20);
        main.getChildren().add(plainTextLabel);
        main.getChildren().add(plainTextField);
        main.getChildren().add(keyLabel);
        main.getChildren().add(keyField);
        main.getChildren().add(cipherLabel);
        main.getChildren().add(cipherText);
        main.getChildren().add(keyBox);          
        
    }

    public void LayoutPrzestawianieMacierzowe3() {
        Label plainTextLabel = new Label("Tekst do zaszyfrowania / rozszyfrowania:");
        Label keyLabel = new Label("Klucz (słowo):");
        Label cipherLabel = new Label("Postać zaszyfrowana / odszyfrowana:");
        Label cipherText = new Label(" - pusty -");
        TextField plainTextField = new TextField();
        plainTextField.setPadding(new Insets(15, 15, 15, 15));
        TextField keyField = new TextField();
        keyField.setPadding(new Insets(15, 15, 15, 15));
        Button doCipher = new Button("Szyfrowanie");
        Button deCipher = new Button("Odszyfrowywanie");
        doCipher.setOnAction(c -> {
            String key = keyField.getText();
            String plainText = plainTextField.getText();
            plainText = plainText.replaceAll("\\s", "");
            String cipher = Matrix.PrzestawianieMacierzowe3(plainText, key);
            cipherText.setText(cipher);
        });
        deCipher.setOnAction(c -> {
            String key = keyField.getText();
            String deCipherText = plainTextField.getText();
            deCipherText = deCipherText.replaceAll("\\s", "");
            String plain = Matrix.unPrzestawianieMacierzowe3(deCipherText, key);
            cipherText.setText(plain);
        });
        HBox keyBox = new HBox();
        keyBox.setSpacing(20);
        keyBox.setAlignment(Pos.CENTER_LEFT);
        keyBox.getChildren().add(doCipher);
        keyBox.getChildren().add(deCipher);
        
        main.setOrientation(Orientation.VERTICAL);
        main.setVgap(20);
        main.getChildren().add(plainTextLabel);
        main.getChildren().add(plainTextField);
        main.getChildren().add(keyLabel);
        main.getChildren().add(keyField);
        main.getChildren().add(cipherLabel);
        main.getChildren().add(cipherText);
        main.getChildren().add(keyBox); 
        
        
    }

    public void LayoutSzyfrCezara() {
        Label plainTextLabel = new Label("Tekst w do zaszyfrowania / odszyfrowania:");
        Label keyLabel = new Label("Klucz (0-26):");
        Label cipherTextLabel = new Label("Postać zaszyfrowana / odszyfrowana:");
        Label cipherText = new Label(" - pusty - ");
        TextField plainTextField = new TextField();
        plainTextField.setPadding(new Insets(15, 15, 15, 15));
        TextField keyTextField = new TextField("10");
        keyTextField.setPadding(new Insets(15, 15, 15, 15));
        keyTextField.prefWidth(50);
        HBox keyBox = new HBox();
        keyBox.setSpacing(10);
        keyBox.setAlignment(Pos.CENTER_LEFT);
        keyBox.getChildren().add(keyLabel);
        keyBox.getChildren().add(keyTextField);
        Button doCipher = new Button("Szyfrowanie");
        Button deCipher = new Button("Odszyfrowanie");
        doCipher.setOnAction(c -> {
            String plainText = plainTextField.getText();
            plainText = plainText.replaceAll("\\s", "");
            int key = Integer.parseInt(keyTextField.getText());
            String cipher = SzyfrCezara.Szyfr(plainText, key);
            cipherText.setText(cipher);
        });
        deCipher.setOnAction(c -> {
            String deCipherText = plainTextField.getText();
            deCipherText = deCipherText.replaceAll("\\s", "");
            int key = Integer.parseInt(keyTextField.getText());
            String cipher = SzyfrCezara.unSzyfr(deCipherText, key);
            cipherText.setText(cipher);
        });
               
        keyBox.setSpacing(20);
        keyBox.setAlignment(Pos.CENTER_LEFT);
        keyBox.getChildren().add(doCipher);
        keyBox.getChildren().add(deCipher);
        
        main.setOrientation(Orientation.VERTICAL);
        main.setVgap(20);
        main.getChildren().add(plainTextLabel);
        main.getChildren().add(plainTextField);
        main.getChildren().add(keyLabel);
        main.getChildren().add(keyTextField);
        main.getChildren().add(keyBox);
        main.getChildren().add(cipherTextLabel);
        main.getChildren().add(cipherText);
        main.getChildren().add(keyBox);        
        
    }

   
    public void LayoutSzyfrowanieVigenere() {
        Label plainTextLabel = new Label("Tekst do zaszyfrowania/odszyfrowania:");
        Label keyLabel = new Label("Klucz (słowo)");
        Label cipherTextLabel = new Label("Tekst w postaci zaszyfrowanej/odszyfrowanej:");
        Label cipherText = new Label(" - pusty - ");
        TextField plainTextField = new TextField();
        plainTextField.setPadding(new Insets(15, 15, 15, 15));
        TextField keyTextField = new TextField();
        keyTextField.setPadding(new Insets(15, 15, 15, 15));
        keyTextField.prefWidth(50);
        Button doCipher = new Button("Szyfrowanie");
        doCipher.setOnAction(c -> {
            String plainText = plainTextField.getText();
            plainText = plainText.replaceAll("\\s", "");
            String key = keyTextField.getText();
            String cipher = SzyfrViegenera.Szyfrowanie(plainText, key);
            cipherText.setText(cipher);
        });
        Button undoCipher = new Button("Odszyfrowywanie");
        undoCipher.setOnAction(c -> {
            String plainText = plainTextField.getText();
            plainText = plainText.replaceAll("\\s", "");
            String key = keyTextField.getText();
            String cipher = SzyfrViegenera.Odszyfrowanie(plainText, key);
            cipherText.setText(cipher);
        });
        
        HBox keyBox = new HBox();
        keyBox.setSpacing(20);
        keyBox.setAlignment(Pos.CENTER_LEFT);
        keyBox.getChildren().add(doCipher);
        keyBox.getChildren().add(undoCipher);

        main.setOrientation(Orientation.VERTICAL);
        main.setVgap(20);
        main.getChildren().add(plainTextLabel);
        main.getChildren().add(plainTextField);
        main.getChildren().add(keyLabel);
        main.getChildren().add(keyTextField);
        main.getChildren().add(keyBox);
        main.getChildren().add(cipherTextLabel);
        main.getChildren().add(cipherText);       
    }

   
    
}
