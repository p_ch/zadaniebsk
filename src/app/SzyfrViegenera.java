/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

/**
 *
 * @author Lenovo
 */
public class SzyfrViegenera {
    
    public SzyfrViegenera(){}
    
     public static String Szyfrowanie (String cText, String key) {
        char[] text = cText.toCharArray();
        StringBuilder cipher = new StringBuilder();
        for (int i = 0, j = 0; i < text.length; i++) {
            if (Character.isUpperCase(text[i])) {
                char znak = (char) (((int)text[i] + Character.toUpperCase(key.charAt(j)) - 2 * 'A') % 26 + 'A');
                cipher.append(znak);
            } else {
                char znak = (char) (((int) text[i] + Character.toLowerCase(key.charAt(j)) - 2 * 'a') % 26 + 'a');
                cipher.append(znak);
            }
            j = ++j % key.length();
        }
        return cipher.toString();

    }

    public static String Odszyfrowanie (String dText, String key) {
        char[] text = dText.toCharArray();
        StringBuilder decipher = new StringBuilder();
        for (int i = 0, j = 0; i < text.length; i++) {
            if (Character.isUpperCase(text[i])) {
                char ch = (char) (((int) text[i] - Character.toUpperCase(key.charAt(j)) + 26) % 26 + 'A');
                decipher.append(ch);
            } else {
                char ch = (char) (((int) text[i] - Character.toLowerCase(key.charAt(j)) + 26) % 26 + 'a');
                decipher.append(ch);
            }
            j = ++j % key.length();
        }
        return decipher.toString();

    }

    
}
