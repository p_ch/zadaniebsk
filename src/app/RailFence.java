/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.lang.reflect.Array;

/**
 *
 * @author piotr
 */
public class RailFence {
    public RailFence () {
        
    }
    
    public static String szyfrowanie (String text, int n) {
        int len = text.length();
        int position;
        boolean direction = true;                                               // true dodawanie, false odejmowanie
        StringBuilder sb = new StringBuilder();
        for(int i = 1; i<=n; i++) {
            position = 1;
            for(int j=0; j<len; j++) {
                if(position == i){
                    sb.append(text.charAt(j));
                }
                if(position == 1 & direction == false) {
                    direction = true;
                    position++;
                }
                else if(position == n & direction == true){
                    direction = false;
                    position--;
                }
                else if(direction){
                    position++;
                }
                else position--;
            }
        }
        
        String szyfr = sb.toString();
        
        return szyfr;
    }
    
    public static String deszyfrowanie (String text, int n) {
        int len = text.length();
        char odp[] = new char[len];
        int pos_text = 0;
        int position;
        boolean direction = true;                                               // true dodawanie, false odejmowanie
        for(int i = 1; i<=n; i++) {
            position = 1;
            for(int j=0; j<len; j++) {
                if(position == i){
                    odp[j] = text.charAt(pos_text);
                    pos_text++;
                }
                if(position == 1 & direction == false) {
                    direction = true;
                    position++;
                }
                else if(position == n & direction == true){
                    direction = false;
                    position--;
                }
                else if(direction){
                    position++;
                }
                else position--;
            }
        }
        
        String tekst = new String(odp);
        return tekst;
    }
}
