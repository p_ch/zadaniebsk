/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author piotr
 */
public class Matrix {

    public static String szyfr(String text, String key) {
        List<String> key_sep = Arrays.asList(key.split("-"));
        int n = key_sep.size();
        int len = text.length();
        int mod = len % n;
        int dim;
        if (mod == 0) {
            dim = len / n;
        } else {
            dim = (len / n) + 1;
        }
        int position = 0;
        System.out.println("Len: " + len + ", Dim: " + dim + ", Mod: " + mod);

        char tab[][] = new char[dim][n];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < n; j++) {
                if (position == len) {
                    break;
                }
                tab[i][j] = text.charAt(position);
                position++;

            }
        }

        position = 0;
        int b;
        char szyfr_tab[] = new char[len];

        for (int j = 0; j < n; j++) {
            b = Integer.parseInt(key_sep.get(j)) - 1;
            for (int i = 0; i < dim; i++) {
                if (mod == 0 || !(mod != 0 && i == dim - 1 && b >= mod)) {
                    szyfr_tab[position] = tab[i][b];
                    position++;
                }
            }
        }
        String wynik = new String(szyfr_tab);
        return wynik;
    }

    public static String deszyfr(String text, String key) {
        List<String> key_sep = Arrays.asList(key.split("-"));
        int n = key_sep.size();
        int len = text.length();
        int mod = len % n;
        int dim;
        if (mod == 0) {
            dim = len / n;
        } else {
            dim = (len / n) + 1;
        }
        int position = 0;
        System.out.println("Len: " + len + ", Dim: " + dim + ", Mod: " + mod);

        char tab[][] = new char[dim][n];

        int b;
        for (int j = 0; j < n; j++) {
            b = Integer.parseInt(key_sep.get(j)) - 1;
            for (int i = 0; i < dim; i++) {
                if (mod == 0 || !(mod != 0 && i == dim - 1 && b >= mod)) {
                    tab[i][b] = text.charAt(position);
                    position++;
                }
            }
        }

        position = 0;
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < n; j++) {
                if (position == len) {
                    break;
                }
                sb.append(tab[i][j]);
                position++;
            }
        }

        String wynik = sb.toString();
        return wynik;
    }

    public static String szyfruj(String text, String key) {
        int key_len = key.length();
        System.out.println("Key lenght: " + key_len);

        // USTALENIE KOLEJNOŚCI \/\/
        char key_tab[] = key.toCharArray();
        int key_ascii[] = new int[key_len];
        for (int i = 0; i < key_len; i++) {
            key_ascii[i] = (int) key_tab[i];
        }
        //System.out.println(new String(key_tab));
        for (int i = 0; i < key_len; i++) {
            System.out.print(key_ascii[i] + " ");
        }
        System.out.println("");
        int key_ascii_copy[] = Arrays.copyOf(key_ascii, key_len);
        Arrays.sort(key_ascii_copy);
        for (int i = 0; i < key_len; i++) {
            System.out.print(key_ascii_copy[i] + " ");
        }
        System.out.println("");
        int q[] = new int[key_len];
        for (int i = 0; i < key_len; i++) {
            for (int j = 0; j < key_len; j++) {
                if (i < key_len - 1 && key_ascii[j] == key_ascii_copy[i]) {
                    q[i] = j;
                    if (key_ascii_copy[i] == key_ascii_copy[i + 1]) {
                        i++;
                        continue;
                    } else {
                        break;
                    }
                } else if (i == key_len - 1 && key_ascii[j] == key_ascii_copy[i]) {
                    q[i] = j;
                }
            }
        }

        for (int i = 0; i < key_len; i++) {
            System.out.print(q[i] + " ");
        }
        System.out.println("");

        // USTALONO KOLEJNOŚĆ /\/\ wpisanie do tablicy tekstu \/\/
        int len = text.length();
        int mod = len % key_len;
        int dim;
        if (mod == 0) {
            dim = len / key_len;
        } else {
            dim = (len / key_len) + 1;
        }
        int position = 0;
        //System.out.println("Len: " + len + ", Dim: " + dim + ", Mod: " + mod);

        char tab[][] = new char[dim][key_len];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < key_len; j++) {
                if (position == len) {
                    break;
                }
                tab[i][j] = text.charAt(position);
                position++;
            }
        }

        // tekst wpisan /\/\ czytanie zgodnie z kolejnością        
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < key_len; j++) {
                System.out.print(tab[i][j] + " ");
            }
            System.out.println("");
        }

        position = 0;
        int b;
        char szyfr_tab[] = new char[len];

        for (int j = 0; j < key_len; j++) {
            b = q[j];
            for (int i = 0; i < dim; i++) {
                if (mod == 0 || !(mod != 0 && i == dim - 1 && b >= mod)) {
                    szyfr_tab[position] = tab[i][b];
                    position++;
                }
            }
        }
        String wynik = new String(szyfr_tab);

        return wynik;
    }

    public static String deszyfruj(String text, String key) {
        int key_len = key.length();

        // USTALENIE KOLEJNOŚCI \/\/
        char key_tab[] = key.toCharArray();
        int key_ascii[] = new int[key_len];
        for (int i = 0; i < key_len; i++) {
            key_ascii[i] = (int) key_tab[i];
        }
        //System.out.println(new String(key_tab));
        //for(int i=0;i<key_len;i++) System.out.println(key_ascii[i]);
        int key_ascii_copy[] = Arrays.copyOf(key_ascii, key_len);
        Arrays.sort(key_ascii_copy);
        //for(int i=0;i<key_len;i++) System.out.println(key_ascii_copy[i]);
        int q[] = new int[key_len];
        for (int i = 0; i < key_len; i++) {
            for (int j = 0; j < key_len; j++) {
                if (i < key_len - 1 && key_ascii[j] == key_ascii_copy[i]) {
                    q[i] = j;
                    if (key_ascii_copy[i] == key_ascii_copy[i + 1]) {
                        i++;
                        continue;
                    } else {
                        break;
                    }
                } else if (i == key_len - 1 && key_ascii[j] == key_ascii_copy[i]) {
                    q[i] = j;
                }
            }
        }

        for (int i = 0; i < key_len; i++) {
            System.out.print(q[i] + " ");
        }
        System.out.println("");

        // USTALONO KOLEJNOŚĆ /\/\ wpisanie do tablicy tekstu \/\/
        int len = text.length();
        int mod = len % key_len;
        int dim;
        if (mod == 0) {
            dim = len / key_len;
        } else {
            dim = (len / key_len) + 1;
        }
        int position = 0;
        //System.out.println("Len: " + len + ", Dim: " + dim + ", Mod: " + mod);

        char tab[][] = new char[dim][key_len];

        // wpisywanie z szyfru \/\/        
        int j;
        for (int a = 0; a < key_len; a++) {
            j = q[a];
            for (int i = 0; i < dim; i++) {
                if (mod == 0 || !(mod != 0 && i == dim - 1 && j >= mod)) {
                    tab[i][j] = text.charAt(position);
                    position++;
                }
            }
        }

        position = 0;
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < dim; i++) {
            for (j = 0; j < key_len; j++) {
                if (position == len) {
                    break;
                }
                sb.append(tab[i][j]);
                position++;
            }
        }

        String wynik = sb.toString();
        return wynik;
    }

    public static String PrzestawianieMacierzowe3(String cText, String tKey) {
        //char[] alphabet = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        char[] text = cText.toLowerCase().toCharArray();
        char[] key = tKey.toLowerCase().toCharArray();
        int keyLength = key.length;
        int exist = 0;
        int[] asciiKey = new int[keyLength];
        //zamiana klucza na ascii
        for (int i = 0; i < keyLength; i++) {
            asciiKey[i] = (int) key[i];
        }
        System.out.println("asciiKey: ");
        for (int i = 0; i < keyLength; i++) {
            System.out.print(asciiKey[i] + " ");
        }
        System.out.println(" ");
        int[] asciiKeySorted = Arrays.copyOf(asciiKey, keyLength);
        Arrays.sort(asciiKeySorted);

        System.out.println("asciiKeySorted: ");
        for (int i = 0; i < keyLength; i++) {
            System.out.print(asciiKeySorted[i] + " ");
        }

        System.out.println(" ");
        int numberKey[] = new int[keyLength];

        for (int i = 0; i < keyLength; i++) {
            for (int j = 0; j < keyLength; j++) {
                if (i < keyLength - 1 && asciiKey[j] == asciiKeySorted[i]) {
                    numberKey[j] = i;
                   if (asciiKeySorted[i] == asciiKeySorted[i + 1]) {
                        i++;
                        continue;
                    } else {
                        break;
                    }
                } else if (i == keyLength - 1 && asciiKey[j] == asciiKeySorted[i]) {
                    numberKey[j] = i;
                }
            }
        }

        for (int i = 0; i < keyLength; i++) {
            System.out.print(numberKey[i] + " ");
        }

        char[][] matrix = new char[key.length][key.length];
        int end = 0;
        int start = 0;
        int endText = 0;
        for (int i = 0; i < key.length; i++) {
            for (int k = 0; k < key.length; k++) {
                if (i == numberKey[k]) {
                    end = k;
                }
            }
//zapis do matix
            for (int j = 0; j <= end; j++) {
                if(start<text.length){
                matrix[i][j] = text[start];
                start = start + 1;
                }
            }

        }
    
        System.out.println(" ");
        for (int i = 0; i < key.length; i++) {
            for (int j = 0; j < key.length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println(" ");
        }
        
        //acsii matrix
        int[][] asciiMatrix = new int[key.length][key.length];
        System.out.println(" ");
        for (int i = 0; i < key.length; i++) {
            for (int j = 0; j < key.length; j++) {
                asciiMatrix[i][j]= (int)matrix[i][j];
            }
            
        }
        
        System.out.println(" ");
        for (int i = 0; i < key.length; i++) {
            for (int j = 0; j < key.length; j++) {
                System.out.print(asciiMatrix[i][j] + " ");
            }
            System.out.println(" ");
        }
        

        StringBuilder cipher = new StringBuilder();
        for (int y = 0; y < key.length; y++) {
            for (int x = 0; x < key.length; x++) {
                char field = matrix[x][y];
                cipher.append(field);

            }
        }
        
        
        return cipher.toString();
    }

    public static String unPrzestawianieMacierzowe3(String dText, String dKey) {
        char[] text = dText.toLowerCase().toCharArray();
        char[] key = dKey.toLowerCase().toCharArray();
        int keyLength = key.length;
        int exist = 0;
        int[] asciiKey = new int[keyLength];
        //zamiana klucza na ascii
        for (int i = 0; i < keyLength; i++) {
            asciiKey[i] = (int) key[i];
        }
        System.out.println("asciiKey: ");
        for (int i = 0; i < keyLength; i++) {
            System.out.print(asciiKey[i] + " ");
        }
        System.out.println(" ");
        int[] asciiKeySorted = Arrays.copyOf(asciiKey, keyLength);
        Arrays.sort(asciiKeySorted);

        System.out.println("asciiKeySorted: ");
        for (int i = 0; i < keyLength; i++) {
            System.out.print(asciiKeySorted[i] + " ");
        }

        System.out.println(" ");
        int numberKey[] = new int[keyLength];

        for (int i = 0; i < keyLength; i++) {
            for (int j = 0; j < keyLength; j++) {
                if (i < keyLength - 1 && asciiKey[j] == asciiKeySorted[i]) {
                    numberKey[j] = i;
                    if (asciiKeySorted[i] == asciiKeySorted[i + 1]) {
                        i++;
                        continue;
                    } else {
                        break;
                    }
                } else if (i == keyLength - 1 && asciiKey[j] == asciiKeySorted[i]) {
                    numberKey[j] = i;
                }
            }
        }

        for (int i = 0; i < keyLength; i++) {
            System.out.print(numberKey[i] + " ");
        }

        char[][] matrix = new char[key.length][key.length];
        int end = 0;
        int start = 0;
        int endText = 0;
        
        for (int j = 0; j < key.length; j++) {
             for (int k = 0; k < key.length; k++) {
                if (j == numberKey[k]) {
                    end = k;
                }
            }
        //zapis do matix
            for (int i = end; i<= end; i++) {                                    
                if(start<text.length){
                matrix[i][j] = text[start];
                start = start + 1;
                }
            }

        }
    
        System.out.println(" ");
        for (int i = 0; i < key.length; i++) {
            for (int j = 0; j < key.length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println(" ");
        }

        StringBuilder cipher = new StringBuilder();
        for (int y = 0; y < key.length; y++) {
            for (int x = 0; x < key.length; x++) {
                char field = matrix[x][y];
                cipher.append(field);

            }
        }
        return cipher.toString();
    }

    /* ROBOCZA
    
    public static String szyfruj_C(String text, String key) {
        int key_len = key.length();
        
        // USTALENIE KOLEJNOŚCI KLUCZA \/\/
        
        char key_tab[] = key.toCharArray();
        int key_ascii[] = new int[key_len];
        for(int i=0; i<key_len; i++) key_ascii[i] = (int)key_tab[i];

        int key_ascii_copy[] = Arrays.copyOf(key_ascii, key_len);
        Arrays.sort(key_ascii_copy);
        
        int q[] = new int[key_len];
        for(int i=0;i<key_len;i++) {
            for(int j=0;j<key_len;j++) {
                if(i<key_len-1 && key_ascii[j] == key_ascii_copy[i]) {
                    q[i] = j;
                    if(key_ascii_copy[i]==key_ascii_copy[i+1]) {
                        i++;
                        continue;
                    } else break;
                }
            }
        }
        
        for(int i=0;i<key_len;i++) System.out.print(q[i] + " ");
        System.out.println("");

        // USTALONO KOLEJNOŚĆ /\/\ wpisanie do tablicy tekstu \/\/

        int len = text.length();
        int mod = len%key_len;
        int dim;
        if(mod==0) {
            dim = len/key_len;
        } else dim = (len/key_len)+1;
        int position = 0;
        //System.out.println("Len: " + len + ", Dim: " + dim + ", Mod: " + mod);
        
        char tab[][] = new char[dim][key_len];
        for(int i = 0; i<dim; i++) {
            for(int j=0; j<key_len; j++) {
                if(position==len) break;
                tab[i][j] = text.charAt(position);
                position++;
            }
        }
        
        // tekst wpisan /\/\ czytanie zgodnie z kolejnością        

        for(int i=0; i<dim; i++) {
            for(int j=0; j<key_len;j++) {
                System.out.print(tab[i][j] + " ");
            }
            System.out.println("");
        }
        
        position = 0;
        int b;
        char szyfr_tab[] = new char[len];
                
        for(int j=0; j<key_len; j++) {
            b = q[j];
            for(int i=0; i<dim; i++) {
                if(mod==0 || !(mod!=0 && i==dim-1 && b>=mod)) {
                    szyfr_tab[position] = tab[i][b];
                    position++;
                }
            }
        }
        String wynik = new String(szyfr_tab);
        
        return wynik;
    }
     */
}
